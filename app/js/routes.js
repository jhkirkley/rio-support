app.config(function AppConfig($routeProvider) {
   $routeProvider
   /*.when("/", {
       templateUrl : "templates/faq.html",
       controller : "faqCtrl",
       controllerAs: 'CTRL'
   }) */
   //put in catchall route
   .when("/support", {
       templateUrl : "templates/support.html",
       controller : "supportCtrl",
       controllerAs: 'CTRL'
   })
   .when("/faq", {
       templateUrl : "templates/faq.html",
       controller : "faqCtrl",
       controllerAs: 'CTRL'
   });


//remove default header configuration
});

app.config(function ($httpProvider) {
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};
});
